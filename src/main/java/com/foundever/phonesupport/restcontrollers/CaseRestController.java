package com.foundever.phonesupport.restcontrollers;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.dtos.CaseDto;
import com.foundever.phonesupport.dtos.CaseMessageDto;
import com.foundever.phonesupport.dtos.MessageDto;
import com.foundever.phonesupport.services.CaseService;
import com.foundever.phonesupport.utils.ResponseEntityBody;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static com.foundever.phonesupport.utils.Constants.Paths.*;

@RestController
@RequestMapping(API)
@Tag(description = "Actor Cases API", name = "case")
public class CaseRestController {
    private final CaseService caseService;
    private MessageDto messageDtoFromMsgClient;

    public CaseRestController(CaseService caseService){
        this.caseService = caseService;
    }


    @Operation(description = "Create Message & Client Case", tags = "case")
    @PostMapping(CREATE_MSG_AND_CLIENT_CASE)
    public ResponseEntity<ResponseEntityBody> createMsgFromAndClientCase(@RequestBody CaseMessageDto caseMessageDto) {
        return this.caseService.createMessageCaseForClient(caseMessageDto);
    }

    @Operation(description = "Reply Message from Support to Client Case", tags = "case")
    @PostMapping(REPLY_MSG_FROM_SUPPORT)
    public ResponseEntity<ResponseEntityBody> replyMessageFromSupport(@RequestBody MessageDto messageDto, @RequestParam Long caseId) {
        return this.caseService.replyMsgFromSupport(messageDto,caseId);
    }

    @Operation(description = "Update Reference to Client Case", tags = "case")
    @PutMapping(UPDATE_REFERENCE_TO_CASE)
    public ResponseEntity<ResponseEntityBody> updateRefToClientCase(@RequestBody CaseDto caseDto) {
        return this.caseService.updateClientCaseReference(caseDto);
    }

    @Operation(description = "Find All Client Cases By Client Id", tags = "case")
    @GetMapping(FIND_ALL_CLIENT_CASES)
    public ResponseEntity<Object> findAllClientCasesById(Long caseId) {
        return this.caseService.getClientCaseById(caseId);
    }
}
