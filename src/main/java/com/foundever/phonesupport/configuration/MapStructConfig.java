
package com.foundever.phonesupport.configuration;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */


import org.mapstruct.MapperConfig;

/**
 * The interface Map struct config.
 */

@MapperConfig(componentModel = "spring")
public interface MapStructConfig {
}
