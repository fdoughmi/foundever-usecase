package com.foundever.phonesupport.services;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.dtos.CaseDto;
import com.foundever.phonesupport.dtos.CaseMessageDto;
import com.foundever.phonesupport.dtos.MessageDto;
import com.foundever.phonesupport.entities.Case;
import com.foundever.phonesupport.utils.ResponseEntityBody;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface CaseService {
    ResponseEntity<ResponseEntityBody> createMessageCaseForClient(CaseMessageDto caseMessageDto);
    ResponseEntity<ResponseEntityBody> replyMsgFromSupport(MessageDto messageDto, Long caseId);
    Case updateClientCase(CaseDto caseDto);
    ResponseEntity<ResponseEntityBody> updateClientCaseReference(CaseDto caseDto);
    ResponseEntity<Object> getClientCaseById(Long caseId);

}
