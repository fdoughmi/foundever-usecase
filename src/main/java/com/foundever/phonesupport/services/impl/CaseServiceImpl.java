package com.foundever.phonesupport.services.impl;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.dtos.CaseDto;
import com.foundever.phonesupport.dtos.CaseMessageDto;
import com.foundever.phonesupport.dtos.MessageDto;
import com.foundever.phonesupport.entities.Case;
import com.foundever.phonesupport.entities.Message;
import com.foundever.phonesupport.mappers.CaseMapper;
import com.foundever.phonesupport.mappers.MessageMapper;
import com.foundever.phonesupport.repositories.CaseRepository;
import com.foundever.phonesupport.services.CaseService;
import com.foundever.phonesupport.utils.CaseStatusEnum;
import com.foundever.phonesupport.utils.DemanderTypeEnum;
import com.foundever.phonesupport.utils.ResponseEntityBody;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;

@Service
public class CaseServiceImpl implements CaseService {
    private final CaseRepository caseRepository;
    private final CaseMapper caseMapper;
    private final MessageMapper messageMapper;

    public CaseServiceImpl(CaseMapper caseMapper,CaseRepository caseRepository,MessageMapper messageMapper){
        this.caseMapper = caseMapper;
        this.caseRepository = caseRepository;
        this.messageMapper = messageMapper;
    }
    @Override
    public ResponseEntity<ResponseEntityBody> createMessageCaseForClient(CaseMessageDto caseMessageDto) {
        if(caseMessageDto.getCaseDto().getCaseId()==null){
            return ResponseEntity.ofNullable(new ResponseEntityBody("This Case Id must not be Null",
                    NOT_ACCEPTABLE));
        }else{
            boolean checkCaseExists = this.caseRepository.findById(caseMessageDto.getCaseDto().getCaseId()).isPresent();
            if(checkCaseExists){
                return ResponseEntity.ofNullable(new ResponseEntityBody("This Case Id is already been used",
                        NOT_ACCEPTABLE));
            }else{
                this.createClientMsg(caseMessageDto.getMessageDto());
                Case caseClient = new Case();
                caseClient.setDateOfCreation(LocalDate.now());
                caseClient.setUpdateDate(LocalDateTime.now());
                caseClient.setCaseStatus(CaseStatusEnum.OPEN);
                List<Message> messageDtoList = new ArrayList<>();
                messageDtoList.add(this.messageMapper.messageDtoToMessage(caseMessageDto.getMessageDto()));
                caseClient.setMessages(messageDtoList);
                this.caseRepository.save(caseClient);
                return ResponseEntity.ok(new ResponseEntityBody("Message & Case Client was created successfully", HttpStatus.OK));
            }
        }
    }

    /**
     * Reply Message From Support
     * ResponseEntity
     * @param messageDto the Message
     * @return ResponseEntity<ResponseEntityBody>
     */
    @Override
    public ResponseEntity<ResponseEntityBody> replyMsgFromSupport(MessageDto messageDto, Long caseId) {
        messageDto.setNameOfDemander("Sonia Valentin");
        messageDto.setMessage("I am Sonia, and I will do my best to help you. What is your phone brand and model?");
        messageDto.setMessageFrom(DemanderTypeEnum.SUPPORT_AGENT.name());
        messageDto.setDateOfCreation(LocalDateTime.now());
        ResponseEntity<ResponseEntityBody> response = this.addAgentSupportMsgToCaseClient(messageDto,caseId);
        return ResponseEntity.ok(
                new ResponseEntityBody(Objects.requireNonNull(response.getBody()).getMessage(),
                        HttpStatus.OK));
    }

    @Override
    public Case updateClientCase(CaseDto caseDto) {
        return this.caseRepository.save(this.caseMapper.caseDtoToCase(caseDto));
    }

    @Override
    public ResponseEntity<ResponseEntityBody> updateClientCaseReference(CaseDto caseDto) {
        if(caseDto.getCaseId()==null){
            return ResponseEntity.ofNullable(new ResponseEntityBody("This Case Id must not be Null",
                    NOT_ACCEPTABLE));
        }else{
            Optional<Case> optCaseToUpdate = this.caseRepository.findById(caseDto.getCaseId());
            if(optCaseToUpdate.isEmpty()){
                return ResponseEntity.ofNullable(new ResponseEntityBody("This Case ID is not existing",
                        NOT_ACCEPTABLE));
            }else{
                Case caseToUpdate = optCaseToUpdate.get();
                caseToUpdate.setReference("KA-18B6");
                this.caseRepository.save(caseToUpdate);
                return ResponseEntity.ok(new ResponseEntityBody("Reference was updated successfully", HttpStatus.OK));
            }

        }
    }

    @Override
    public ResponseEntity<Object> getClientCaseById(Long caseId) {
        if(caseId!= null){
            Optional<Case> optionalCase = this.caseRepository.findById(caseId);
            return optionalCase.<ResponseEntity<Object>>map(ResponseEntity::ok)
                    .orElseGet(() -> ResponseEntity.ofNullable(new ResponseEntityBody("There is no records !",
                            NOT_ACCEPTABLE)));
        }else{
            return ResponseEntity.ofNullable(new ResponseEntityBody("Case Id must not be null !",
                    NOT_ACCEPTABLE));
        }
    }

    public void createClientMsg(MessageDto messageDto){
        messageDto.setNameOfDemander("Jeremie Durand");
        messageDto.setMessage("Hello, I have an issue with my new phone");
        messageDto.setMessageFrom(DemanderTypeEnum.CLIENT.name());
        messageDto.setDateOfCreation(LocalDateTime.now());
    }

    /**
     * Add Agent Support Message to Previous Client Case
     * @param messageDto : Message DTO
     */
    public ResponseEntity<ResponseEntityBody> addAgentSupportMsgToCaseClient(MessageDto messageDto,Long caseId){
        Optional<Case> optionalCase= this.caseRepository.findById(caseId);
        if(optionalCase.isPresent()){
            Case existingCase = optionalCase.get();
            List<Message> messageDtoList = existingCase.getMessages();
            messageDtoList.add(this.messageMapper.messageDtoToMessage(messageDto));
            existingCase.setMessages(messageDtoList);
            Case caseSuccess = this.updateClientCase(this.caseMapper.caseToCaseDto(existingCase));
            return ResponseEntity.ok(
                    new ResponseEntityBody("Case "+caseSuccess.getCaseId()+" updated successfully",
                            HttpStatus.OK));
        }else{
            return ResponseEntity.ofNullable(new ResponseEntityBody("The case is not existing",
                    NOT_ACCEPTABLE));
        }
    }
}
