package com.foundever.phonesupport.utils;

/*
 * Copyright (c) 2020 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

public enum DemanderTypeEnum {
    CLIENT("CLIENT"), SUPPORT_AGENT("SUPPORT_AGENT");

    DemanderTypeEnum(String demander) {}

    @Override
    public String toString() {
        return name();
    }
}
