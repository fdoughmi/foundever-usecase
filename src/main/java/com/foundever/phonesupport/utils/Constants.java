package com.foundever.phonesupport.utils;

/*
 * Copyright (c) 2020 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

public class Constants {
    public static class Paths {
        public static final String API="/api";
        public static final String FIND_ALL_CLIENT_CASES="/findAllClientCases";
        public static final String CREATE_MSG_AND_CLIENT_CASE="/createMsgClientCase";
        public static final String REPLY_MSG_FROM_SUPPORT="/replyMsgFromSupport";
        public static final String UPDATE_REFERENCE_TO_CASE="/updateRefToCaseById";

    }
}
