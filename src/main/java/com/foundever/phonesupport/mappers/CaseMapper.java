
package com.foundever.phonesupport.mappers;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.configuration.MapStructConfig;
import com.foundever.phonesupport.dtos.CaseDto;
import com.foundever.phonesupport.entities.Case;
import org.mapstruct.Mapper;

@Mapper(config = MapStructConfig.class)
public interface CaseMapper {

    /**
     * Case to Case Dto.
     * @param caseObj the Case
     * @return the case dto
     */
    CaseDto caseToCaseDto(Case caseObj);

    /**
     * Case Dto To Case
     * @param caseDto the Case dto
     * @return the Case
     */
    Case caseDtoToCase(CaseDto caseDto);
}
