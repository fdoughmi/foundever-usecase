package com.foundever.phonesupport.entities;

/*
 * Copyright (c) 2020 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Table(name="messages")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long messageId= 0L;
    private String nameOfDemander;
    private String messageFrom;
    private String message;
    private LocalDateTime dateOfCreation;

    public Message(Long messageId, String nameOfDemander, String messageFrom, String message, LocalDateTime dateOfCreation) {
        this.messageId = messageId;
        this.nameOfDemander = nameOfDemander;
        this.messageFrom = messageFrom;
        this.message = message;
        this.dateOfCreation = dateOfCreation;
    }
}
