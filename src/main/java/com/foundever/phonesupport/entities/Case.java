package com.foundever.phonesupport.entities;

/*
 * Copyright (c) 2024 by FD, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.dtos.MessageDto;
import com.foundever.phonesupport.utils.CaseStatusEnum;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name="cases")
public class Case {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long caseId = 0L;
    private String reference;
    private CaseStatusEnum caseStatus;
    @OneToMany(cascade = CascadeType.ALL)
    private List<Message> messages;
    private LocalDate dateOfCreation;
    private LocalDateTime updateDate;

    public Case(Long caseId, String reference, CaseStatusEnum caseStatus, List<Message> messages, LocalDate dateOfCreation, LocalDateTime updateDate) {
        this.caseId = caseId;
        this.reference = reference;
        this.caseStatus = caseStatus;
        this.messages = messages;
        this.dateOfCreation = dateOfCreation;
        this.updateDate = updateDate;
    }
}
