package com.foundever.phonesupport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhoneSupportBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhoneSupportBackendApplication.class, args);
	}

}
