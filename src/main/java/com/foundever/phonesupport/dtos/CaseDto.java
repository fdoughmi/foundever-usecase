package com.foundever.phonesupport.dtos;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.utils.CaseStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
public class CaseDto {
    private Long caseId;
    private String reference;
    private CaseStatusEnum caseStatus;
    private List<MessageDto> messages;
    private LocalDate dateOfCreation;
    private LocalDateTime updateDate;

    public CaseDto(Long caseId, String reference, CaseStatusEnum caseStatus, List<MessageDto> messages,
                   LocalDate dateOfCreation, LocalDateTime updateDate) {
        this.caseId = caseId;
        this.reference = reference;
        this.caseStatus = caseStatus;
        this.messages = messages;
        this.dateOfCreation = dateOfCreation;
        this.updateDate = updateDate;
    }
}
