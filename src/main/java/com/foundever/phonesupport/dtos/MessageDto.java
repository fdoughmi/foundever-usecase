package com.foundever.phonesupport.dtos;

/*
 * Copyright (c) 2024 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.utils.DemanderTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
public class MessageDto {
    private Long messageId;
    private String nameOfDemander;
    private String messageFrom;
    private String message;
    private LocalDateTime dateOfCreation;

    public MessageDto(Long messageId, String nameOfDemander, String messageFrom, String message, LocalDateTime dateOfCreation) {
        this.messageId = messageId;
        this.nameOfDemander = nameOfDemander;
        this.messageFrom = messageFrom;
        this.message = message;
        this.dateOfCreation = dateOfCreation;
    }
}
