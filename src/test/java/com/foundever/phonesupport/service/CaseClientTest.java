package com.foundever.phonesupport.service;

/*
 * Copyright (c) 2020 by FDCorp, Inc., All rights reserved.
 * This source code, and resulting software, is the confidential and proprietary information
 * ("Proprietary Information") and is the intellectual property ("Intellectual Property")
 * of FDCorp Group, Inc. ("The Company"). You shall not disclose such Proprietary Information and
 * shall use it only in accordance with the terms and conditions of any and all license
 * agreements you have entered into with The Company.
 */

import com.foundever.phonesupport.dtos.CaseDto;
import com.foundever.phonesupport.dtos.CaseMessageDto;
import com.foundever.phonesupport.dtos.MessageDto;
import com.foundever.phonesupport.entities.Case;
import com.foundever.phonesupport.mappers.CaseMapper;
import com.foundever.phonesupport.mappers.MessageMapper;
import com.foundever.phonesupport.repositories.CaseRepository;
import com.foundever.phonesupport.services.CaseService;
import com.foundever.phonesupport.services.impl.CaseServiceImpl;
import com.foundever.phonesupport.utils.CaseStatusEnum;
import com.foundever.phonesupport.utils.ResponseEntityBody;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class CaseClientTest {
    private CaseService caseService;
    CaseRepository caseRepository = mock(CaseRepository.class);
    CaseMapper caseMapper = mock(CaseMapper.class);
    MessageMapper messageMapper = mock(MessageMapper.class);
    MessageDto messageDto;
    CaseDto caseDto;
    CaseMessageDto caseMessageDto;

    @BeforeEach
    void setUp() {
        messageDto = new MessageDto();
        caseDto = new CaseDto();
        this.caseService = new CaseServiceImpl(caseMapper, caseRepository,messageMapper);
    }

    @Test
    void testCreateMessageCaseForClientOk(){
        Case caseClient =this.caseService.updateClientCase(new CaseDto(1L,"ref1", CaseStatusEnum.OPEN,
                null, LocalDate.now(), LocalDateTime.now()));
        caseDto.setCaseId(1L);
        when(caseRepository.findById(1L)).thenReturn(Optional.ofNullable(caseClient));
        caseMessageDto= new CaseMessageDto(caseDto,messageDto);
        ResponseEntity<ResponseEntityBody> response= this.caseService.createMessageCaseForClient(caseMessageDto);
        HttpStatus statusCode= Objects.requireNonNull(response.getBody()).getHttpStatus();
        Assertions.assertEquals(HttpStatus.OK, statusCode);
    }

    @Test
    void testCreateMessageCaseForClientKo(){
        caseMessageDto= new CaseMessageDto(caseDto,messageDto);
        ResponseEntity<ResponseEntityBody> response= this.caseService.createMessageCaseForClient(caseMessageDto);
        HttpStatus statusCode= Objects.requireNonNull(response.getBody()).getHttpStatus();
        Assertions.assertEquals(HttpStatus.NOT_ACCEPTABLE, statusCode);
    }

    @Test
    void testReplyMsgFromSupportOk(){
        Case caseClient = this.caseService.updateClientCase(new CaseDto(1L,"ref1", CaseStatusEnum.OPEN,
                null, LocalDate.now(), LocalDateTime.now()));
        when(caseRepository.findById(1L)).thenReturn(Optional.ofNullable(caseClient));
        caseDto.setCaseId(1L);
        caseMessageDto= new CaseMessageDto(caseDto,messageDto);
        ResponseEntity<ResponseEntityBody> response= this.caseService.replyMsgFromSupport(caseMessageDto.getMessageDto(),
                caseMessageDto.getCaseDto().getCaseId());
        HttpStatus statusCode= Objects.requireNonNull(response.getBody()).getHttpStatus();
        Assertions.assertEquals(HttpStatus.OK, statusCode);
    }

    @Test
    void testReplyMsgFromSupportKo(){
        caseMessageDto= new CaseMessageDto(caseDto,messageDto);
        ResponseEntity<ResponseEntityBody> response= this.caseService.createMessageCaseForClient(caseMessageDto);
        HttpStatus statusCode= Objects.requireNonNull(response.getBody()).getHttpStatus();
        Assertions.assertEquals(HttpStatus.NOT_ACCEPTABLE, statusCode);
    }

    @Test
    void testUpdateClientCaseReferenceOk(){
        Case caseClient = this.caseService.updateClientCase(new CaseDto(1L,"ref1", CaseStatusEnum.OPEN,
                null, LocalDate.now(), LocalDateTime.now()));
        when(caseRepository.findById(1L)).thenReturn(Optional.ofNullable(caseClient));
        caseDto.setCaseId(1L);
        ResponseEntity<ResponseEntityBody> response= this.caseService.updateClientCaseReference(caseDto);
        HttpStatus statusCode= Objects.requireNonNull(response.getBody()).getHttpStatus();
        Assertions.assertNotEquals(HttpStatus.OK, statusCode);
    }

    @Test
    void testUpdateClientCaseReferenceKo(){
        ResponseEntity<ResponseEntityBody> response= this.caseService.updateClientCaseReference(caseDto);
        HttpStatus statusCode= Objects.requireNonNull(response.getBody()).getHttpStatus();
        Assertions.assertEquals(HttpStatus.NOT_ACCEPTABLE, statusCode);
    }
}
